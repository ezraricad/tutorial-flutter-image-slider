import 'package:flutter/material.dart';
import 'package:flutter_app_imageslider/entity/imageslider.dart';
import 'package:flutter_app_imageslider/util/carousel_slider.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'dart:async' show Future;

class BerandaPage extends StatefulWidget {
  @override
  _BerandaPageState createState() => new _BerandaPageState();
}

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }

  return result;
}

class _BerandaPageState extends State<BerandaPage> {
//  List<GojekService> _gojekServiceList = [];
  int _current = 0;

  ScrollController controller;
  int limit = 0;
  int offset = 10;
  List<ImageSliderEntity> _friends = [];
  List _imgListWidget = [];
  List<String> _imgList2 = [];

  @override
  void initState() {
    super.initState();
    _loadBannerUI();
    setState(() { });
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body:
           new SafeArea(
            child: new Scaffold(
              appBar: AppBar(
                title: Text('Images Slider'),
              ),
              body: new Container(
                child: new ListView(
                  physics: ClampingScrollPhysics(),
                  children: <Widget>[
                    Container(
                        padding: EdgeInsets.only(left: 5.0, right: 5.0, top: 5.0),
                        color: Colors.white,
                        child: new Column(
                          children: <Widget>[
                            _imgListWidget.isNotEmpty ?
                            _buildImageSliderEntity(_imgListWidget, _imgList2) :  Center(
                                child: CircularProgressIndicator(),
                              ),
                          ],
                        )),
                  ],
                ),
              ),
            ),
      ),
    );
  }

  List<T> mapss<T>(List list, Function handler) {
    List<T> result = [];

    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  Future<List> _loadBannerUI() async {
    http.Response response =
    await http.get('http://simplesolutionapps.com/APISample/hut24byr/get_imagesslider_tutorial');
    _friends = ImageSliderEntity.allFromResponse(response.body);

//    var xx = response.body;
//    var users = new List<Employee>();

//    List<Widget> list = new List<Widget>();
    for(var i = 0; i < _friends.length; i++){
      _imgList2.add(_friends[i].path);
    }

    _imgListWidget = mapss<Widget>(
      _imgList2,
          (index, i) {
        return Container(
          margin: EdgeInsets.all(5.0),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            child: Stack(children: <Widget>[
              Image.network(i, fit: BoxFit.cover, width: 1000.0),
              Positioned(
                bottom: 0.0,
                left: 0.0,
                right: 0.0,
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [Color.fromARGB(200, 0, 0, 0), Color.fromARGB(0, 0, 0, 0)],
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                    ),
                  ),
                  padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                ),
              ),
            ]),
          ),
        );
      },
    ).toList();

    return _imgListWidget;

  }

  @override
  Widget _buildImageSliderEntity(List _banners, List imgListBanner) {

    return Column(children: [
      CarouselSlider(
        items: _banners,
        autoPlay: true,
        enlargeCenterPage: true,
        viewportFraction: 1.0,
        aspectRatio: 2.0,
        onPageChanged: (index) {
          setState(() {
            _current = index;
          });
        },
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: map<Widget>(
          imgListBanner,
              (index, url) {
            return Container(
              width: 8.0,
              height: 8.0,
              margin: EdgeInsets.symmetric(vertical: 1.0, horizontal: 2.0),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == index
                      ? Theme.of(context).primaryColor
                      :  Colors.grey),
            );
          },
        ),
      ),
    ]);
  }


}