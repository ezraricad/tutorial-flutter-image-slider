import 'dart:convert';
import 'package:meta/meta.dart';

class ImageSliderEntity {
  ImageSliderEntity({
    @required this.idblog,
    @required this.path,
    @required this.createdby,
    @required this.createdtime,
  });

  final String idblog;
  final String path;
  final String createdby;
  final String createdtime;

  static List<ImageSliderEntity> allFromResponse(String response) {
    var decodedJson = json.decode(response).cast<String, dynamic>();

    return decodedJson['aaData']
        .cast<Map<String, dynamic>>()
        .map((obj) => ImageSliderEntity.fromMap(obj))
        .toList()
        .cast<ImageSliderEntity>();
  }


  static ImageSliderEntity fromMap(Map map) {

    return new ImageSliderEntity(
      idblog: map['idblog'],
      path : map['path'],
      createdby: map['createdby'],
      createdtime: map['createdtime'],
    );
  }
  

  static String _capitalize(String input) {
    return input.substring(0, 1).toUpperCase() + input.substring(1);
  }
}
